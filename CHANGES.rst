.. _changelog:

Changelog
---------

This project follows `SemVer <https://semver.org/>`_.


.. _upcoming:

Upcoming (unreleased)
^^^^^^^^^^^^^^^^^^^^^

Added
~~~~~
- Support images with only namespace
- Support update of images with only namespace
- Support adding custom ``ade enter`` command through environment variable
- Experimental support for OSX
- Add option to specify a different ADE configuration file

Changed
~~~~~~~
- Start ADE volumes without a network connection
- Reprompt for token when authentication fails with existing token

Deprecated
~~~~~~~~~~

Removed
~~~~~~~

Fixed
~~~~~
- Support Docker versions ending in -ce
- Support images with only namespace
- Fix ``ade start --enter`` for devices with broken ``docker exec``
- Suppress misleading error message on startup
- Update images chosen with 'select' option
- Add note in documentation about making adeinit scripts executable
- Fix error message that described how to debug a failed ``ade start``

Security
~~~~~~~~


.. _v4.1.0:

4.1.0 (2020-01-26)
^^^^^^^^^^^^^^^^^^^^^

Added
~~~~~
- Add support for ``--gpus`` option in Docker 19.03+

Fixed
~~~~~
- Support images from registries with explicit port specification
- Fix nvidia-docker 1 support


.. _v4.0.0:

4.0.0 (2019-11-18)
^^^^^^^^^^^^^^^^^^

Added
~~~~~
- Enable self-update from ADE releases on GitLab
- Support saving and loading images to run on offline machines

Changed
~~~~~~~
- Switch to single-binary as default means of distribution, using pyinstaller and staticx

Removed
~~~~~~~
- Drop support for Python 3.5 and 3.6, instead Python 3.7 is required for development


.. _v3.5.1:

3.5.1 (2019-09-26)
^^^^^^^^^^^^^^^^^^

Added
~~~~~
- Make host location of ade-home known inside container
  `#39 <https://gitlab.com/ApexAI/ade-cli/issues/39>`_
- Document usage of ADE with the fork+pull model
  `#19 <https://gitlab.com/ApexAI/ade-cli/issues/19>`_
- Document how to build custom base images and volumes
  `#50 <https://gitlab.com/ApexAI/ade-cli/issues/50>`_
- Document how to run ADE with ``--net=host``
  `#33 <https://gitlab.com/ApexAI/ade-cli/issues/33>`_

Fixed
~~~~~
- Let ade enter return with exit code of shell running within container
  `#46 <https://gitlab.com/ApexAI/ade-cli/issues/46>`_
- Ask for API scope credentials when prompting for Gitlab API token
  `#42 <https://gitlab.com/ApexAI/ade-cli/issues/42>`_
- Update instructions to address aarch64 installation issues
  `#23 <https://gitlab.com/ApexAI/ade-cli/issues/23>`_


.. _v3.5.0:

3.5.0 (2019-08-08)
^^^^^^^^^^^^^^^^^^

Added
~~~~~
- Add note about starting ADE with --net=host
- Add support for NVidia Docker 2
  `#31 <https://gitlab.com/ApexAI/ade-cli/issues/31>`_
- Add initial set of documentation
- Extend ``ade start --help`` to include information on publishing ports


.. _v3.4.1:

3.4.1 (2018-11-16)
^^^^^^^^^^^^^^^^^^

Changed
~~~~~~~
- Pinned aiohttp<3.5.0 for compatibility with Python 3.5.2


.. _v3.4.0:

3.4.0 (2018-11-16)
^^^^^^^^^^^^^^^^^^

Changed
~~~~~~~
- Print n/a in image matrix for images that have not been pulled


.. _v3.3.1:

3.3.1 (2018-11-15)
^^^^^^^^^^^^^^^^^^

Added
~~~~~
- Add environment variables to introspect loaded image versions
- Print git commit or tag info in image matrix
- Add CI job for release validation and publication

.. _v3.2.0:

3.2.0 (2018-11-14)
^^^^^^^^^^^^^^^^^^

Added
~~~~~
- Add ``--version`` option
- Add nvidia-docker support
- Add workaround for broken ``docker exec``
- Add tests and initial CI jobs
- Add ADE_CLI_VERSION environment variables
- Add infrastructure to add requirements for development on ade-cli

Changed
~~~~~~~
- Renamed ``--check`` to ``--update``
- Pin to release Click 7.0
- Code cleanup


.. _v3.1.0:

3.1.0 (2018-10-10)
^^^^^^^^^^^^^^^^^^

Added
~~~~~
- Support setting docker run args via configuration

Fixed
~~~~~
- Fix passing docker run arguments to ade start


.. _v3.0.1:

3.0.1 (2018-09-11)
^^^^^^^^^^^^^^^^^^

Fixed
~~~~~
- Fix homepage and project URLs


.. _v3.0.0:

3.0.0 (2018-09-04)
^^^^^^^^^^^^^^^^^^

Added
~~~~~
- Initial public release of ADE
