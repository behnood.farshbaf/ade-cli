.. _offline-install:

Offline Installation
--------------------

``ade-cli`` provides facilities to install ADE (``ade-cli``, ADE base image,
and ADE volumes) on a machine without an internet connection:

1. On a machine with an internet connection and the same architecture as the
   target machine, run::

     $ cd adehome/path/to/.aderc
     $ ade start
     $ ade save ./offline-install
     $ tar cf ade-offline-install.tar ../offline-install

2. Copy ``ade-offline-install.tar`` to the target machine 
3. On the target machine, run::

     $ tar xf ade-offline-install.tar
     $ mv ./offline-install/ade path/in/PATH
     $ mv offline-install ~/ade-home
     $ cd ~/ade-home
     $ ade load .
     $ touch .adehome
     $ ade start
